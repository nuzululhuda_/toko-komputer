<?php

use Illuminate\Database\Migrations\Migration;

class CreateEccompDetailSalesOrderView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_detail_sales_order");
        DB::statement("CREATE VIEW eccomp_detail_sales_order AS SELECT  eccomp_transactions.id, 
        eccomp_transactions.`date`, eccomp_customers.name AS customer_name, eccomp_customers.address AS customer_address, 
        eccomp_products.code AS product_code, eccomp_products.name AS product_name, eccomp_products.sell_price, eccomp_transaction_details.qty, 
        eccomp_transaction_details.total_price
        FROM eccomp_transactions 
        INNER JOIN eccomp_transaction_details ON eccomp_transactions.id = eccomp_transaction_details .eccomp_transaction_id 
        INNER JOIN eccomp_customers ON eccomp_transactions.eccomp_customer_id = eccomp_customers.id
        INNER JOIN eccomp_products ON eccomp_transaction_details.eccomp_product_code = eccomp_products.code"); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_detail_dales_order");
    }
}
