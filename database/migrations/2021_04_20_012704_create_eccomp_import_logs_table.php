<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompImportLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eccomp_import_logs', function (Blueprint $table) {
            $table->id();
            $table->string('import_name');
            $table->integer('total_data');
            $table->integer('success_import')->default(0);
            $table->integer('error_import')->default(0);
            $table->dateTime('done_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eccomp_import_logs');
    }
}
