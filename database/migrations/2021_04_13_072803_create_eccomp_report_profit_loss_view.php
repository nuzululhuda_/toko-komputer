<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompReportProfitLossView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_report_profit_loss");
        DB::statement("CREATE VIEW eccomp_report_profit_loss AS SELECT SUM(etd.qty * eccomp_products.purchase_price) as total_purchase_price, SUM(etd.qty * etd.unit_price) as total_sell_price, DATE_FORMAT(eccomp_transactions.date, '%Y-%m-%d') as date_transaction FROM 
            eccomp_transaction_details etd 
            INNER JOIN eccomp_products ON etd.eccomp_product_code = eccomp_products.code
            INNER  JOIN eccomp_transactions  ON etd.eccomp_transaction_id = eccomp_transactions.id  
            GROUP BY date_transaction");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_report_profit_loss");
    }
}
