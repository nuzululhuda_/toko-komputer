<?php

use Illuminate\Database\Migrations\Migration;

class CreateAfterTransactionUpdateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP TRIGGER IF EXISTS after_transaction_update");
        DB::unprepared("CREATE TRIGGER `after_transaction_update` AFTER UPDATE ON `eccomp_transaction_details` FOR EACH ROW BEGIN 
            IF EXISTS (SELECT eccomp_products.stock FROM eccomp_products WHERE code = NEW.eccomp_product_code AND stock-(NEW.qty-OLD.qty) >= 0) THEN 
                UPDATE eccomp_products SET stock = eccomp_products.stock - NEW.qty 
                WHERE eccomp_products.code = NEW.eccomp_product_code;
            ELSE
                SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Cannot insert transaction insufficient stock';
            END IF;
        END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_transaction_update`');
    }
}
