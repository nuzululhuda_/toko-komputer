<?php

use Illuminate\Database\Migrations\Migration;

class CreateEccompDetailStockProductView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_detail_stock_product");
        DB::statement("CREATE VIEW eccomp_detail_stock_product AS SELECT  eccomp_transactions.date, eccomp_transaction_details.eccomp_product_code, SUM(eccomp_transaction_details.qty) as total_sold,
        SUM(eccomp_purchase_order_details.qty) as total_in
        FROM eccomp_transaction_details
        INNER JOIN eccomp_transactions ON eccomp_transactions.id = eccomp_transaction_details.eccomp_transaction_id 
        INNER JOIN eccomp_products ON eccomp_transaction_details.eccomp_product_code = eccomp_products.code 
        INNER JOIN eccomp_purchase_order_details ON eccomp_transaction_details.eccomp_product_code = eccomp_purchase_order_details.eccomp_product_code 
        GROUP BY eccomp_transactions.date, eccomp_transaction_details.eccomp_product_code");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_detail_stock_product");
    }
}
