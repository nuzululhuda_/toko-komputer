<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompPurchaseOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eccomp_purchase_order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('eccomp_purchase_order_id');
            $table->string('eccomp_product_code');
            $table->integer('qty');
            $table->integer('total_price');
            $table->timestamps();

            $table->foreign('eccomp_purchase_order_id')
                ->references('id')
                ->onDelete('cascade')
                ->on('eccomp_purchase_orders');

            $table->foreign('eccomp_product_code')
                ->references('code')
                ->onDelete('cascade')
                ->on('eccomp_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eccomp_purchase_order_details');
    }
}
