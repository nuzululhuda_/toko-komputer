<?php

use Illuminate\Database\Migrations\Migration;

class CreateAfterTransactionInsertTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP TRIGGER IF EXISTS after_transaction_insert");
        DB::unprepared("CREATE TRIGGER `after_transaction_insert` AFTER INSERT ON `eccomp_transaction_details` FOR EACH ROW BEGIN 
            IF EXISTS (SELECT eccomp_products.stock FROM eccomp_products WHERE stock >= NEW.qty AND code = NEW.eccomp_product_code) THEN 
                UPDATE eccomp_products SET stock = eccomp_products.stock - NEW.qty 
                WHERE eccomp_products.code = NEW.eccomp_product_code;
            ELSE
                SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Cannot insert transaction insufficient stock';
            END IF;
        END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_transaction_insert`');
    }
}
