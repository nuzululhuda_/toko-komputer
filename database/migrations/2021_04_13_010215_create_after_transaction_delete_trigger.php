<?php

use Illuminate\Database\Migrations\Migration;

class CreateAfterTransactionDeleteTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP TRIGGER IF EXISTS after_transaction_delete");
        DB::unprepared("CREATE TRIGGER `after_transaction_delete` AFTER DELETE ON `eccomp_transaction_details` FOR EACH ROW BEGIN 
            UPDATE eccomp_products SET eccomp_products.stock = (eccomp_products.stock + OLD.qty) 
            WHERE eccomp_products.code  = OLD.eccomp_product_code;
        END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_transaction_delete`');
    }
}
