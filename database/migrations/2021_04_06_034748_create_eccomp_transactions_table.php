<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eccomp_transactions', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('eccomp_customer_id');
            $table->unsignedBigInteger('eccomp_user_id');
            $table->timestamps();

            $table->foreign('eccomp_customer_id')
                ->references('id')
                ->on('eccomp_customers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('eccomp_user_id')
                ->references('id')
                ->on('eccomp_users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
