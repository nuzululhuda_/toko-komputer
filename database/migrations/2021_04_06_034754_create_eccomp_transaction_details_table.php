<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eccomp_transaction_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('eccomp_transaction_id');
            $table->string('eccomp_product_code');
            $table->integer('qty');
            $table->integer('unit_price');
            $table->integer('total_price');
            $table->timestamps();

            $table->foreign('eccomp_transaction_id')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade')
                ->on('eccomp_transactions');

            $table->foreign('eccomp_product_code')
                ->references('code')
                ->on('eccomp_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
