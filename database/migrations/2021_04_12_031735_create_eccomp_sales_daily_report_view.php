<?php

use Illuminate\Database\Migrations\Migration;

class CreateEccompSalesDailyReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_sales_daily_report");
        DB::statement("CREATE VIEW eccomp_sales_daily_report AS 
            SELECT date, SUM(eccomp_transaction_details.total_price) AS total 
            FROM eccomp_transactions 
            INNER JOIN eccomp_transaction_details ON eccomp_transactions.id = eccomp_transaction_details.eccomp_transaction_id 
            GROUP BY date");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW eccomp_sales_daily_report");
    }
}
