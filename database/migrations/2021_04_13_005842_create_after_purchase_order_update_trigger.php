<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfterPurchaseOrderUpdateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP TRIGGER IF EXISTS after_purchase_order_update");
        DB::unprepared("CREATE TRIGGER `after_purchase_order_update` AFTER UPDATE ON `eccomp_purchase_order_details` FOR EACH ROW BEGIN 
        IF EXISTS (SELECT * FROM eccomp_products WHERE code = OLD.eccomp_product_code AND stock-(OLD.qty-NEW.qty) >= 0) THEN
            UPDATE eccomp_products SET eccomp_products.stock = (eccomp_products.stock - OLD.qty + NEW.qty)
            WHERE eccomp_products.code = OLD.eccomp_product_code;
            ELSE
            SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Cannot update purchase order';
            END IF;
        END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_purchase_order_update`');
    }
}
