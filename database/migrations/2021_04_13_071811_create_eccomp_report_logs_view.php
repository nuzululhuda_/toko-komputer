<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompReportLogsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_report_logs");
        DB::statement("CREATE VIEW eccomp_report_logs AS SELECT  etd.eccomp_product_code, CONCAT('penjualan') as type, eccomp_transactions.date as date_transaction, etd.qty 
            FROM eccomp_transaction_details as etd
            INNER JOIN eccomp_transactions ON etd.eccomp_transaction_id = eccomp_transactions.id 
                UNION 
            SELECT epod.eccomp_product_code, CONCAT('pembelian') as type, eccomp_purchase_orders.date as date_transaction, epod.qty
            FROM eccomp_purchase_order_details as epod 
            INNER JOIN eccomp_purchase_orders ON epod.eccomp_purchase_order_id = eccomp_purchase_orders.id
            ORDER BY date_transaction");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_report_logs");
    }
}
