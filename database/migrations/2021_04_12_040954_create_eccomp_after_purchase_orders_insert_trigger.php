<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateEccompAfterPurchaseOrdersInsertTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP TRIGGER IF EXISTS after_purchase_order_insert");
        DB::unprepared("CREATE TRIGGER after_purchase_order_insert
        AFTER INSERT 
        ON eccomp_purchase_order_details FOR EACH ROW
        BEGIN 
            UPDATE eccomp_products SET eccomp_products.stock = (eccomp_products.stock + NEW.qty) 
            WHERE eccomp_products.code  = NEW.eccomp_product_code;
        END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_purchase_order_insert`');
    }
}
