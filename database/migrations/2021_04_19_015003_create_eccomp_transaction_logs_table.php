<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEccompTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eccomp_transaction_logs', function (Blueprint $table) {
            $table->id();
            $table->string('eccomp_product_code')->index();
            $table->date('date');
            $table->integer('total_purchase');
            $table->integer('total_sales');
            $table->string('type');
            $table->unsignedBigInteger('detail_transaction_id')->index();
            $table->integer('stock');
            $table->timestamps();

            $table->foreign('eccomp_product_code')
                ->references('code')
                ->on('eccomp_products')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eccomp_transaction_logs');
    }
}
