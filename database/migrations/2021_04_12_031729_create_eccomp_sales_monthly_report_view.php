<?php

use Illuminate\Database\Migrations\Migration;

class CreateEccompSalesMonthlyReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_sales_monthly_report");
        DB::statement("CREATE VIEW eccomp_sales_monthly_report AS
            SELECT COUNT('id') AS total, eccomp_products.category, MONTH(eccomp_transactions.date)  AS month_trans
            FROM eccomp_transaction_details 
            INNER JOIN eccomp_products ON eccomp_products.code = eccomp_transaction_details.eccomp_product_code 
            INNER  JOIN eccomp_transactions ON eccomp_transaction_details.eccomp_transaction_id = eccomp_transactions.id
            GROUP BY  eccomp_products.category, month_trans");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW eccomp_sales_monthly_report");
    }
}
