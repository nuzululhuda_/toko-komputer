<?php

use Illuminate\Database\Migrations\Migration;

class CreateEccompDetailPurchaseOrderView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_detail_purchase_order");
        DB::statement("CREATE VIEW eccomp_detail_purchase_order AS SELECT  eccomp_purchase_orders.id, eccomp_purchase_orders.`date`,
        eccomp_products.code, eccomp_products.sell_price, eccomp_purchase_order_details.qty, eccomp_purchase_order_details.total_price 
        FROM eccomp_purchase_orders 
        INNER JOIN eccomp_purchase_order_details ON eccomp_purchase_orders.id = eccomp_purchase_order_details.eccomp_purchase_order_id 
        INNER JOIN eccomp_products ON eccomp_purchase_order_details.eccomp_product_code = eccomp_products.code");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS eccomp_detail_purchase_order");
    }
}
