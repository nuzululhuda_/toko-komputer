<?php

namespace Database\Seeders;

use App\Models\EccompUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class EccompUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where(['name' => 'administrator'])->first();
        $salesRole = Role::where(['name' => 'sales'])->first();
        $purchaseRole = Role::where(['name' => 'purchasing'])->first();

        $admin = EccompUser::create([
            'name' => 'Administrator',
            'email' => 'admin@eccomp.net',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
            'remember_token' => Str::random(10)
        ]);

        $sales = EccompUser::create([
            'name' => 'Sales',
            'email' => 'sales@eccomp.net',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
            'remember_token' => Str::random(10)
        ]);

        $purchasing = EccompUser::create([
            'name' => 'Purchasing',
            'email' => 'purchasing@eccomp.net',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
            'remember_token' => Str::random(10)
        ]);

        $admin->assignRole($adminRole);
        $sales->assignRole($salesRole);
        $purchasing->assignRole($purchaseRole);
    }
}
