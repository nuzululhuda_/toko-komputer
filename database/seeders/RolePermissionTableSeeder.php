<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create(['name' => 'administrator']);
        $salesRole = Role::create(['name' => 'sales']);
        $purchaseRole = Role::create(['name' => 'purchasing']);

        $salesPermission = Permission::create(['name' => 'sales']);
        $purchasePermission = Permission::create(['name' => 'purchasing']);
        Permission::create(['name' => 'report_profit']);
        Permission::create(['name' => 'logs']);
        Permission::create(['name' => 'report_transaction']);

        $salesRole->givePermissionTo($salesPermission);
        $salesPermission->assignRole($salesRole);
        
        $purchaseRole->givePermissionTo($purchasePermission);
        $purchasePermission->assignRole($purchaseRole);

        $permissionsAdmin = Permission::get();
        $adminRole->syncPermissions($permissionsAdmin);
  
    }
}
