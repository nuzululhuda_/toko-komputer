<?php

namespace Database\Seeders;

use App\Models\EccompCustomer;
use App\Models\EccompProduct;
use App\Models\EccompPurchaseOrder;
use App\Models\EccompPurchaseOrderDetail;
use Illuminate\Database\Seeder;
use App\Models\EccompTransaction;
use App\Models\EccompTransactionDetail;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolePermissionTableSeeder::class);
        $this->call(EccompUserTableSeeder::class);
        
        EccompCustomer::factory(50)->create();
        EccompProduct::factory(50)->create();
        
        EccompPurchaseOrder::factory(50)
            ->has(EccompPurchaseOrderDetail::factory()->count(3))
            ->create();

        EccompTransaction::factory(50)
            ->has(EccompTransactionDetail::factory()->count(3))
            ->create();
            
    }
}
