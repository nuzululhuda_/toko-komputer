<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EccompProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class EccompProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EccompProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sellPrice = [2000000, 5000000, 10000000, 15000000];
        $price = $sellPrice[rand(0, 3)];
        $advantage = 100000;
        return [
            'code' => strtoupper(Str::random(3)), 
            'name' => $this->faker->word, 
            'sell_price' => $price, 
            'purchase_price' => $price - $advantage, 
            'stock' => 0, 
            'category' => EccompProduct::CATEGORIES[rand(0, 4)]
        ];
    }
}
