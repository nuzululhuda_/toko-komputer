<?php

namespace Database\Factories;

use App\Models\EccompProduct;
use App\Models\EccompTransactionDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class EccompTransactionDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EccompTransactionDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $product = EccompProduct::inRandomOrder()->where('stock', '>', 2)->first();
        $listQty = [4, 3, 2];
        $qty = $listQty[rand(0,2)];
        
        return [
            'eccomp_product_code' => $product->code, 
            'qty' => $qty, 
            'unit_price' => $product->sell_price, 
            'total_price' => $product->sell_price * $qty
        ];
    }
}
