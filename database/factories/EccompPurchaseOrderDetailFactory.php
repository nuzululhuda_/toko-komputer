<?php

namespace Database\Factories;

use App\Models\EccompProduct;
use App\Models\EccompPurchaseOrderDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class EccompPurchaseOrderDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EccompPurchaseOrderDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $product = EccompProduct::inRandomOrder()->first();
        $listQty = [30, 40, 50];
        $qty = $listQty[rand(0, 2)];

        return [
            'eccomp_product_code' => $product->code, 
            'qty' => $qty, 
            'total_price' => $product->purchase_price * $qty
        ];
    }
}
