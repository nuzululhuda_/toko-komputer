<?php

namespace Database\Factories;

use App\Models\EccompUser;
use App\Models\EccompCustomer;
use App\Models\EccompTransaction;
use Illuminate\Database\Eloquent\Factories\Factory;

class EccompTransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EccompTransaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $customer = EccompCustomer::inRandomOrder()->first();
        $user = EccompUser::permission('sales')->inRandomOrder()->first();
        return [
            'date' => $this->faker->dateTimeBetween('-20 days', '+1 days'), 
            'eccomp_customer_id' => $customer->id,
            'eccomp_user_id' => $user->id
        ];
    }
}
