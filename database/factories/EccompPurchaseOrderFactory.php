<?php

namespace Database\Factories;

use App\Models\EccompUser;
use App\Models\EccompPurchaseOrder;
use Illuminate\Database\Eloquent\Factories\Factory;

class EccompPurchaseOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EccompPurchaseOrder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = EccompUser::permission('purchasing')->inRandomOrder()->first();
        return [
            'date' => $this->faker->dateTimeBetween('-30 days', '+1 days'), 
            'eccomp_user_id' => $user->id
        ];
    }
}
