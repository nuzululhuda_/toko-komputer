@extends('app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="container">

    <div class="card">
        <div class="card-header">
            Laporan Profit Transaksi
        </div>
        <div class="card-body">
            <table id="profit-transaction" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Total Pembelian</th>
                        <th>Total Penjualan</th>
                        <th>Total Profit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($profitTransaction as $transaction)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($transaction->date_transaction)->format('Y-m-d') }}</td>
                        <td>{{ number_format($transaction->total_purchase_price, 2, ',', '.') }}</td>
                        <td>{{ number_format($transaction->total_sell_price, 2, ',', '.') }}</td>
                        <td>{{ number_format($transaction->total_sell_price-$transaction->total_purchase_price, 2, ',', '.') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#profit-transaction').DataTable({
            ordering: false,
            columnDefs: [
                { width: "60px", targets: 3 }
            ]
        });
    })
</script>    
@endsection