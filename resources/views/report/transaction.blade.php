@extends('app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="container">
    
    <div class="card">
        <div class="card-header">
            Laporan Transaksi
        </div>
        <div class="card-body">
            <table id="product" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                       <th>Nama Customer</th>
                       <th>Alamat Customer</th>
                       <th>Kode Produk</th>
                       <th>Nama Produk</th>
                       <th>Harga Jual</th>
                       <th>QTY</th>
                       <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactionDetail as $transaction)
                    <tr>
                        <td>{{ $transaction->customer_name }}</td>
                        <td>{{ $transaction->customer_address }}</td>
                        <td>{{ $transaction->product_code }}</td>
                        <td>{{ $transaction->product_name }}</td>
                        <td>{{ number_format($transaction->sell_price, 2, ',', '.') }}</td>
                        <td>{{ $transaction->qty }}</td>
                        <td>{{ number_format($transaction->total_price, 2, ',', '.') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
            $('#product').DataTable({
                ordering: false
            });
        } );
</script>
@endsection