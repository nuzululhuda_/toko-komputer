@extends('app')
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">@endsection
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Laporan Kartu Stok
        </div>
        <div class="card-body">
            <div class="form-group">
                <label>Produk</label>
                <select name="product" class="form-control select2" >
                    @foreach ($products as $product)
                        <option value="{{ $product->code }}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary">Cek</button>
            </div>
        </div>
    </div>
    <div class="card d-none stock-card">
        <div class="card-body stock-card-data">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(".select2").select2({
        theme: 'bootstrap4'
    })
    $('button').click(function () {
        $.ajax({       
            url : "{{ route('products.stock-card') }}",
            type: 'POST',
            datatype : "JSON",
            data: {
                "_token": "{{ csrf_token() }}",
                "product_code": $("select[name=product]").val()
            },
            success: function (response) {
                $(".stock-card").removeClass("d-none");
                $(".stock-card-data").html(`<table class="table table-bordered">
                    <thead>
                        <th>Tanggal</th>
                        <th>Pembelian</th>
                        <th>Penjualan</th>
                        <th>Sisa</th>
                    </thead>
                    <tbody>
                       ${Object.keys(response).map(function (key) {
                            return `<tr>
                                <td>${key}</td>
                                <td>${response[key].total_pembelian}</td>
                                <td>${response[key].total_penjualan}</td>
                                <td>${response[key].stock}</td>
                            </tr>`
                        }).join("")}
                    </tbody>
                </table>`);
            }, error: function (xhr, status, error) {
                console.log(error);
            }
            
        });
    })
</script>
@endsection