@extends('app')
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col">
            <div class="card border-primary" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Total Penjualan Hari Ini</h5>
                    <p>{{ number_format($salesOrderToday, 2, ',', '.') }}</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-warning" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Total Pembelian Hari Ini</h5>
                    <p>{{ number_format($purchaseOrderToday, 2, ',', '.') }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header text-left"><b>Penjualan</b> Bulan Ini</div>
                <div class="card-body">
                    <canvas id="barChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header text-left"><b>Penjualan</b> Presentase Per Kategori</div>
                <div class="card-body">
                    <div class="card-body">
                        <canvas id="pieChart" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-left"><b>Penjualan</b> 6 Bulan Terakhir</div>
                <div class="card-body">
                    <canvas id="lineChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <h4 class="text-center">10 penjualan Terakhir</h4>
    <table class="table table-stripped table-hovered">
        <thead>
            <tr>
                <th>Nama Customer</th>
                <th>Alamat Customer</th>
                <th>Tanggal Penjualan</th>
                <th>Total Penjualan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($latestTransaction as $transaction)

            <tr>
                <td>{{ $transaction->customer->name }}</td>
                <td>{{ $transaction->customer->address }}</td>
                <td>{{ \Carbon\Carbon::parse($transaction->date)->format('Y-m-d') }}</td>
                <td>{{ number_format($transaction->total,2,',','.') }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.0.2/chart.min.js"
        integrity="sha512-dnUg2JxjlVoXHVdSMWDYm2Y5xcIrJg1N+juOuRi0yLVkku/g26rwHwysJDAMwahaDfRpr1AxFz43ktuMPr/l1A=="
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $.ajax({
            url: "{{ route('dashboard.chart.daily') }}",
            method: "GET",
            success: function(data) {
                let label = [];
                let value = [];
                for (let i in data) {
                    label.push(data[i].date);
                    value.push(data[i].total);
                }
                var bar = document.getElementById('barChart');
                let chart = new Chart(bar, {
                    type: 'bar',
                    data: {
                        labels: label,
                        datasets: [{
                            label: 'Grafik Penjualan Harian',
                            backgroundColor: [
                                '#ff6384', '#36a2eb', '#cc65fe', '#ffce56'
                            ],
                            borderColor: 'rgb(255, 255, 255)',
                            
                            data: value
                        }]
                    },
                    
                    
                    options: {}
                });
            }
        });

        $.ajax({
            url: "{{ route('dashboard.chart.prosentase') }}",
            method: "GET",
            success: function(data) {
                let label = [];
                let value = [];
                for (let i in data) {
                    label.push(data[i].category);
                    value.push(data[i].prosentase);
                }
                var pie = document.getElementById('pieChart');
                let chart = new Chart(pie, {
                    type: 'pie',
                    data: {
                        labels: label,
                        datasets: [{
                            label: 'Prosentase Penjualan Berdasar Kategori',
                            backgroundColor: [
                                '#ff6384', '#36a2eb', '#cc65fe', '#ffce56'
                            ],
                            borderColor: 'rgb(255, 255, 255)',
                            data: value
                        }]
                    },
                    options: {}
                });
            }
        });
        $.ajax({
            url: "{{ route('dashboard.chart.sixmonth') }}",
            method: "GET",
            success: function(data) {
                let label = [];
                let value = [];
                for (let i in data) {
                    label.push(data[i].date);
                    value.push(data[i].total);
                }
                var line = document.getElementById('lineChart');
                let chart = new Chart(line, {
                    type: 'bar',
                    data: {
                        labels: label,
                        datasets: [{
                            label: 'Data Penjualan 6 Bulan Terakhir',
                            backgroundColor: [
                                '#ff6384', '#36a2eb', '#cc65fe', '#ffce56'
                            ],
                            borderColor: 'rgb(255, 255, 255)',
                            data: value
                        }]
                    },
                    options: {}
                });
            }
        });
    });
        
                
</script>
@endsection