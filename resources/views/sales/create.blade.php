@extends('app')
@section('css')
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
@endsection
@section('content')
<div class="container">
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first() }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h4>Tambah Transaksi Penjualan</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('sales.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Tanggal</label>
                    <input autocomplete="off" type="text" class="form-control datepicker @error('tanggal') is-invalid @enderror"
                        name="tanggal">
                    @error('tanggal')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Customer</label>
                    <select name="customer" class="form-control select2 @error('customer') is-invalid @enderror">
                        <option value="">Pilih Customer</option>
                        @foreach ($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endforeach
                    </select>
                    @error('customer')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="button" onclick="addProduct()" class="btn btn-primary">
                        <span class="fa fa-plus"></span>
                        Tambah Produk
                    </button>
                </div>

                <div id="product">
                </div>
                <div class="form-group" id="total">
                    <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-2" id="total"></div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary d-none save">
                        <span class="fa fa-save"></span>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
<script>
    var counter = 1;
            $(".datepicker").datepicker({
                autoclose: true,
                todayHighlight: true
            })
            $(".select2").select2({
                theme: 'bootstrap4'
            })

        function addProduct() {
            $("#product").append(`<div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputCity">Produk</label>
                    <select class="form-control select2" required name="produk[]" onchange="setProduct(`+counter+`)" id="product`+counter+`">
                        <option>Pilih produk</option>
                        @foreach($products as $product)
                            <option value="{{ $product->code }}" >{{ $product->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="inputState">Stock</label>
                    <input type="text" id="max_qty`+counter+`" class="form-control" readonly>
                </div>
                <div class="form-group col-md-1">
                    <label for="inputState">QTY</label>
                    <input type="number" value="1" required id="qty`+counter+`" onkeyup="changeQty(`+counter+`)" name="qty[]" class="form-control">
                </div>
                <div class="form-group col-md-1">
                    <label>Diskon(%)</label>
                    <input type="number" value="0" required id="discount`+counter+`" onkeyup="addDiscount(`+counter+`)" name="discount[]" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputZip">Total Price</label>
                    <input type="text" id="money`+counter+`" name="price[]" readonly class="form-control">
                </div>
                <input type="hidden" id="price`+counter+`">
                <div class="form-group col-md-1">
                    <span class="fa fa-trash pt-5 pl-2 removerow"></span>
                </div>
            </div>`);

            $(".select2").select2({
                theme: 'bootstrap4'
            });
            counter++

            if (counter > 1) {
                $(".save").removeClass('d-none')
            }

        }

        function setProduct(number) {
            let product = $("#product"+number).val()
            if (product != '') {
                $.get("{{ route('dashboard.index') }}/products/"+product)
                .done(function( data ) {
                    let qty = $("#qty"+number).val()
                    if (isNaN(qty) || qty == '' || parseInt(qty) < 1) { qty=0 }
                    $("#money"+number).val(formatRupiah(parseInt(data.sell_price) *parseInt(qty)))
                    $("#price"+number).val(data.sell_price)
                    $("#max_qty"+number).val(data.stock)
                    calculateTotal()
                });
            }
        }

        function changeQty(number) {
            let qty = $("#qty"+number).val()
            let stock = $("#max_qty"+number).val()
            let discount = $("#discount"+number).val()
            
            if (qty > stock) {
               $("#qty"+number).val(0)
            }
            
            let price = $("#price"+number).val();
            
            if (isNaN(qty) || qty == '' || parseInt(qty) < 1) {
                qty = 0
            }

            if (price == '') {
                price = 0
            }           

            if (discount == '') {
                discount = 0
            }           

            let total = parseInt(price) * parseInt(qty)
            let discountTotal = (parseInt(discount) / 100) * total

            $("#money"+number).val(formatRupiah(total-discountTotal))
            calculateTotal()
        }

        function addDiscount(number) {
            let qty = $("#qty"+number).val()
            let price = $("#price"+number).val();
            let discount = $("#discount"+number).val()

            if (discount > 100) {
                $("#discount"+number).val(100)
            }
            
            if (isNaN(qty) || qty == '' || parseInt(qty) < 1) {
                qty = 0
            }

            if (price == '') {
                price = 0
            }           
            if (discount == '') {
                discount = 0
            }           

            let total = parseInt(price) * parseInt(qty)
            let discountTotal = (parseInt(discount) / 100) * total

            $("#money"+number).val(formatRupiah(total-discountTotal))
            calculateTotal()
        }

        function formatRupiah(angka){
			return new Intl.NumberFormat("id-ID", {  currency: "IDR" }).format(angka);
		}

        function calculateTotal(){
            var values = $("input[name='price[]']")
              .map(function(){
                  return parseInt($(this).val().replace(/\./g,''))
                }).get()
            let totalPrice = values.reduce((a, b) => a + b, 0)
            $("#total").html("Total Price : "+formatRupiah(totalPrice))
        }

        $("#product").on("click", ".removerow", function () {
            $(this).closest('.form-row').remove();
            counter--

            if (counter == 1) {
                $(".save").addClass('d-none')
            }
        });
        
</script>
@endsection