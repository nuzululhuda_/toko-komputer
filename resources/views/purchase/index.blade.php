@extends('app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="container">
    @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first() }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card border-primary" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Total Transaksi Pembelian</h5>
                    <p>{{ number_format($totalTransaction, 2, ',', '.') }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Data Pembelian
            <div class="float-right">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    <span class="fa fa-file-excel"></span>
                    Import Data Pembelian
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Import Data Transaksi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form enctype="multipart/form-data" id="uploadimport">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">File</label>
                                        <input type="file" class="form-control-file" name="file">
                                    </div>
                                    <div class="form-group">
                                        <a target="_blank" href="{{ asset('template/purchase-order-template.xlsx') }}">Download Template</a>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="btn-prev" class="btn btn-primary">Preview</button>
                                    </div>
                                    <table class="table table-bordered d-none" id="result-table">
                                        <tr>
                                            <td>Total</td>
                                            <td>
                                                <p id="total"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Valid</td>
                                            <td>
                                                <p id="valid"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Invalid</td>
                                            <td>
                                                <p id="invalid"></p>
                                            </td>
                                        </tr>
                                    </table>
                                    <ul class="nav nav-tabs d-none" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab"
                                                href="#valid-data" role="tab" aria-controls="home"
                                                aria-selected="true">Valid Data</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#invalid-data"
                                                role="tab" aria-controls="profile" aria-selected="false">Invalid
                                                Data</a>
                                        </li>

                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="valid-data" role="tabpanel"
                                            aria-labelledby="home-tab">
                                            <div id="valid-detail"></div>
                                        </div>
                                        <div class="tab-pane fade" id="invalid-data" role="tabpanel"
                                            aria-labelledby="profile-tab">
                                            <div id="invalid-detail"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="filename">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="btn-save" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('sales.create') }}" class="btn btn-primary">
                    <span class="fa fa-plus"></span>
                    Tambah Transaksi Pembelian
                </a>
            </div>
        </div>
        <div class="card-body">
            <table id="purchase" class="table table-stripped table-hovered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Total Pembelian</th>
                        <th>Admin</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($purchases as $purchase)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($purchase->date)->format('d/m/Y') }}</td>
                        <td>{{ number_format($purchase->eccompPurchaseOrderDetail->sum('total_price'), 2, ',', '.') }}
                        </td>
                        <td>{{ $purchase->author->name }}</td>
                        <td>
                            <div>
                                <a href="{{ route('purchases.show', $purchase->id) }}">
                                    <span class="fa fa-eye"></span>
                                </a>
                                <a href="{{ route('purchases.edit', $purchase->id) }}">
                                    <span class="fa fa-edit"></span>
                                </a>
                                <a href="#">
                                    <span class="fa fa-trash delete" data-id="{{ $purchase->id }}"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin untuk menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <form method="POST" action="{{ route('purchases.destroy') }}">
                    <input type="hidden" name="id">
                    @method('DELETE')
                    @csrf
                    <button href="#" class="btn btn-danger" onclick="event.preventDefault();
                                                                                    this.closest('form').submit();">
                        {{ __('Hapus') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#purchase').DataTable({
            ordering: false,
            columnDefs: [
                { width: "60px", targets: 3 }
            ]
        });

        $("#uploadimport").submit(function (event) {
            event.preventDefault()
            var formData = new FormData($(this)[0]);
            console.log(formData)
            $.ajax({       
                url : "{{ route('purchases.upload.import') }}",
                type: 'POST',
                datatype : "JSON",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $("input[name=filename]").val(response.filename)
                    $("#total").html(response.total)
                    $("#valid").html(response.valid)
                    $("#invalid").html(response.invalid)
                    $("#result-table").removeClass('d-none')
                    $("#myTab").removeClass('d-none')
                    $("#valid-detail").html(`
                        <table class="table">
                            <thead>
                                <th>Tanggal</th>
                                <th>Kode Produk</th>
                                <th>Qty</th>
                            </thead>
                            <tbody>
                            ${response.valid_data.map(item => 
                                `<tr>
                                    <td>${item.date}</td>
                                    <td>${item.product_code}</td>
                                    <td>${item.qty}</td>
                                </tr>`
                            ).join('')}
                            </tbody>
                        </table>
                    `)
                    $("#invalid-detail").html(`
                        <table class="table">
                            <thead>
                                <th>Tanggal</th>
                                <th>Kode Produk</th>
                                <th>Qty</th>
                            </thead>
                            <tbody>
                                ${response.invalid_data.map(item =>
                                `<tr>
                                    <td>${item.date}</td>
                                    <td>${item.product_code}</td>
                                    <td>${item.qty}</td>
                                </tr>`
                                ).join('')}
                            </tbody>
                        </table>
                    `)
                }, error: function (xhr, status, error) {
                    console.log(error);
                }
            });
            
        });
    })

    $("#btn-save").click(function () {
        $.ajax({
            url: "{{ route('purchases.execute.import') }}",
            type: 'POST',
            datatype : "JSON",
            data: {
                "_token": "{{ csrf_token() }}",
                "filename": $("input[name=filename]").val()
            },
            success: function (response) {
                window.location.href = "/dashboard/purchases"
            }
        })
    })

    $(".delete").click(function () {
        let id = $(this).data('id')
        $("input[name=id]").val(id)
        $("#modalDelete").modal("show")
    })
</script>
@endsection