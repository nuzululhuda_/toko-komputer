@extends('app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h4>Detail Pembelian</h4>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <td>Tanggal</td>
                    <td>{{ \Carbon\Carbon::parse($purchase->date)->format('d/m/Y') }}</td>
                </tr>
              
                <tr>
                    <td>Admin</td>
                    <td>{{ $purchase->author->name }}</td>
                </tr>
            </table>
            <table class="table table-bordered">
                <thead>
                    <th>Produk</th>
                    <th>Qty</th>
                    <th>Stok Produk Saat Ini</th>
                    <th>Harga </th>
                    <th>Total </th>
                </thead>
                <tbody>
                    @foreach ($purchase->eccompPurchaseOrderDetail as $item)
                        <tr>
                            <td>{{ $item->product->name }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ $item->product->stock }}</td>
                            <td>{{ number_format($item->product->purchase_price, 2, ',', '.') }}</td>
                            <td>{{ number_format($item->total_price, 2, ',', '.') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection