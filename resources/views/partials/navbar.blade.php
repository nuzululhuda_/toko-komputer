<nav class="navbar  navbar-expand-lg navbar-dark bg-primary mb-4">
    <div class="container">
        <a class="navbar-brand" href="{{ route('dashboard.index') }}">Ecommerce Computer</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @can('purchasing')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('products.index') }}">
                        Produk
                    </a>
                </li>
                @endcan
                
                @can('sales')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('sales.index') }}">
                        Penjualan
                    </a>
                </li>
                @endcan
                @can('purchasing')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('purchases.index') }}">
                        Pembelian
                    </a>
                </li>
                @endcan
                @can('logs', 'report_transaction')
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Laporan
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('report.profit') }}">Laporan Laba </a>
                        <a class="dropdown-item" href="{{ route('report.transaction') }}">Laporan Transaksi</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('report.stock-card') }}">Kartu Stock</a>
                    </div>
                </li>
                @endcan
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ auth()->user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        
                <form method="POST" action="{{ route('logout') }}">
                    <li class="dropdown-item">
                    @csrf
                    <a href="#" onclick="event.preventDefault();
                                                                    this.closest('form').submit();">
                        {{ __('Log out') }}
                    </a>

                    </li>
                </form>
        </div>
        </li>
        </ul>
    </div>
    </div>
</nav>