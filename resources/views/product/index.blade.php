@extends('app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
    <div class="container">
        @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                Data Produk
                <a href="{{ route('products.create') }}" class="btn btn-primary float-right">Tambah Produk</a>
            </div>
            <div class="card-body">
                <table id="product" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Kode Produk</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th>Stok</th>
                            <th>Kategori</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach ($products as $product)
                           <tr>
                               <td>{{ $product->name }}</td>
                               <td>{{ $product->code }}</td>
                               <td>{{ number_format($product->purchase_price, 2, ',', '.') }}</td>
                               <td>{{ number_format($product->sell_price, 2, ',', '.') }}</td>
                               <td>{{ $product->stock }}</td>
                               <td>{{ $product->category }}</td>
                               <th>
                                    <a href="{{ route('products.edit', $product->id) }}" class="btn-btn-warning btn-xs">
                                        <span class="fas fa-edit"></span>
                                    </a>
                               </th>
                           </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#product').DataTable({
                ordering: false
            });
        } );
    </script>
@endsection