@extends('app')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Edit Produk
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('products.update', $product->id) }}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label>Nama Produk</label>
                    <input type="text" name="nama_produk" value="{{ $product->name }}"
                        class="form-control @error('nama_produk') is-invalid @enderror" placeholder="Nama Produk">
                    @error('nama_produk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Kode Produk</label>
                    <input type="text" name="kode_produk" value="{{ $product->code }}"
                        class="form-control @error('kode_produk') is-invalid @enderror"
                        placeholder="Kode Produk (unik)">
                    @error('kode_produk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Harga Beli</label>
                    <input type="text" name="harga_beli" value="{{ $product->purchase_price }}"
                        class="form-control purchase_price @error('harga_beli') is-invalid @enderror"
                        placeholder="Harga Beli">
                    @error('harga_beli')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Harga Jual</label>
                    <input type="text" name="harga_jual" value="{{ $product->sell_price }}"
                        class="form-control sell_price @error('harga_jual') is-invalid @enderror"
                        placeholder="Harga Jual">
                    @error('harga_jual')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Kategori</label>
                    <select name="kategori" class="form-control @error('kategori') is-invalid @enderror">
                        @foreach ($categories as $category)
                        <option value="{{ $category }}" {{ $product->category == $category ? 'selected' : '' }}>
                            {{ $category }}
                        </option>
                        @endforeach
                    </select>
                    @error('kategori')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
<script>
    // The options are...optional :)
        const autoNumericOptions = {
            digitGroupSeparator : '.',
            decimalCharacter : ',',
            decimalCharacterAlternative: '.',
            unformatOnSubmit: true
        };
        
        // Initialization
        new AutoNumeric('.purchase_price', autoNumericOptions);
        new AutoNumeric('.sell_price', autoNumericOptions);
</script>
@endsection