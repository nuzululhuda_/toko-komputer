<?php

namespace App\Http\Requests\Purchase;

use App\Rules\Purchase\ValidToDeletePurchaseOrder;
use Illuminate\Foundation\Http\FormRequest;

class DeletePurchaseOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['bail', 'required', 'exists:eccomp_purchase_orders', new ValidToDeletePurchaseOrder()]
        ];
    }
}
