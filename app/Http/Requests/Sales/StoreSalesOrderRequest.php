<?php

namespace App\Http\Requests\Sales;

use Illuminate\Foundation\Http\FormRequest;

class StoreSalesOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'required|date',
            'customer' => 'required|exists:eccomp_customers,id',
            'produk' => 'required|array|min:1',
            'produk.*' => 'required|exists:eccomp_products,code',
            'qty' => 'required|array|min:1',
            'qty.*' => 'required|int|min:1'
        ];
    }
}
