<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_produk' => 'required|string|min:2',
            'kode_produk' => 'required|string|min:2|unique:eccomp_products,code',
            'harga_beli' => 'required|integer|min:10',
            'harga_jual' => 'required|integer|min:10',
            'kategori' => 'required|string'
        ];
    }
}
