<?php

namespace App\Http\Controllers;

use App\Models\EccompProduct;
use App\Models\EccompTransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * view report transaction
     *
     * @return void
     */
    public function transaction()
    {
        $transactionDetail = DB::table('eccomp_detail_sales_order')->get();
        return view('report.transaction', compact('transactionDetail'));
    }

    /**
     * view report stock card
     *
     * @return void
     */
    public function stockCard()
    {
        $products = EccompProduct::get();
        return view('report.stock-card', compact('products'));
    }
    
    /**
     * view report profitability
     *
     * @return void
     */
    public function profit()
    {
        $profitTransaction = DB::table('eccomp_report_profit_loss')->get();
        return view('report.profit', compact('profitTransaction'));
    }
}
