<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EccompPurchaseOrder;
use App\Http\Requests\import\UploadImportRequest;
use App\Http\Requests\import\ExecuteImportRequest;
use App\Http\Requests\Purchase\DeletePurchaseOrderRequest;
use App\Import\ImportPurchaseOrder;

class PurchaseOrderController extends Controller
{
    private $importPurchaseOrder;

    /**
     * constructor
     *
     * @param ImportPurchaseOrder $importPurchaseOrder
     */
    public function __construct(ImportPurchaseOrder $importPurchaseOrder)
    {
        $this->importPurchaseOrder = $importPurchaseOrder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = EccompPurchaseOrder::with(['author', 'eccompPurchaseOrderDetail'])
            ->orderBy('id', 'desc')
            ->get();

        $totalTransaction = 0;
        $purchases->map(function ($item) use(&$totalTransaction) {
            $totalTransaction += $item->eccompPurchaseOrderDetail->sum('total_price');
        });

        return view('purchase.index', compact('purchases', 'totalTransaction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = EccompPurchaseOrder::with(['author', 'eccompPurchaseOrderDetail.product'])
            ->where('id', $id)
            ->firstOrFail();

        return view('purchase.show', compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeletePurchaseOrderRequest $request)
    {
        EccompPurchaseOrder::find($request->id)->delete();
        return redirect()->back()->with('success', 'Hapus penjualan berhasil');
    }

    /**
     * Upload import
     *
     * @param UploadImportRequest $request
     * @return void
     */
    public function uploadImport(UploadImportRequest $request)
    {
        $upload = $this->importPurchaseOrder->uploadImport($request);

        return response()->json($upload);
    }

    /**
     * Execute import
     *
     * @param ExecuteImportRequest $request
     * @return void
     */
    public function executeImport(ExecuteImportRequest $request)
    {
        $execute = $this->importPurchaseOrder->executeImport($request);

        return response()->json($execute);
    }
}
