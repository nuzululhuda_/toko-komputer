<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EccompProduct;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = EccompProduct::orderBy('id', 'desc')->get();
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = EccompProduct::CATEGORIES;
        return view('product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = EccompProduct::create([
            'name' => $request->nama_produk,
            'code' => $request->kode_produk,
            'purchase_price' => $request->harga_beli,
            'sell_price' => $request->harga_jual,
            'category' => $request->kategori,
        ]);
        return redirect()->route('products.index')->with('success', 'Produk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $product = EccompProduct::where('code', $code)->firstOrFail();
        $product->price_formated = number_format($product->sell_price, 0, ',', '.');
        
        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = EccompProduct::findOrFail($id);
        $categories = EccompProduct::CATEGORIES;
        return view('product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = EccompProduct::whereId($id)->update([
            'name' => $request->nama_produk,
            'code' => $request->kode_produk,
            'purchase_price' => $request->harga_beli,
            'sell_price' => $request->harga_jual,
            'category' => $request->kategori,
        ]);
        return redirect()->route('products.index')->with('success', 'Edit produk berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Report stock card
     *
     * @return void
     */
    public function stockCard(Request $request)
    {
        $stockCard = DB::table('eccomp_report_logs')
            ->where('eccomp_product_code', $request->product_code)
            ->get();
        
        $dateStockCard = $stockCard->unique('date_transaction')->pluck('date_transaction');
        $newStockCard = [];
        $totalStock = 0;
        foreach ($dateStockCard as $date) {
            $newStockCard[$date] = [
                'total_pembelian' => 0,
                'total_penjualan' => 0,
                'stock' => $totalStock
            ];
            $stocks = $stockCard->where('date_transaction', $date);
            foreach ($stocks as $stock) {
                if ($stock->type == 'pembelian') {
                    $totalStock += $stock->qty; 
                    $newStockCard[$date]['total_pembelian'] = $newStockCard[$date]['total_pembelian'] + $stock->qty;
                    $newStockCard[$date]['stock'] = $totalStock;
                }else {
                    $totalStock -= $stock->qty; 
                    $newStockCard[$date]['total_penjualan'] = $newStockCard[$date]['total_penjualan'] + $stock->qty;
                    $newStockCard[$date]['stock'] = $totalStock;
                }
            }
        }
        return response()->json($newStockCard);
    }
}
