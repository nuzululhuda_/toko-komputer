<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\EccompProduct;
use App\Models\EccompCustomer;
use App\Import\ImportSalesOrder;
use App\Models\EccompTransaction;
use Illuminate\Support\Facades\DB;
use App\Models\EccompTransactionDetail;
use App\Http\Requests\import\UploadImportRequest;
use App\Http\Requests\import\ExecuteImportRequest;
use App\Http\Requests\Sales\StoreSalesOrderRequest;
use App\Http\Requests\Sales\DeleteSalesOrderRequest;
use App\Http\Requests\Sales\UpdateSalesOrderRequest;

class SalesOrderController extends Controller
{

    private $importSalesOrder;

    /**
     * custroctor
     *
     * @param ImportSalesOrder $importSalesOrder
     */
    public function __construct(ImportSalesOrder $importSalesOrder)
    {
        $this->importSalesOrder = $importSalesOrder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = EccompTransaction::with(['customer', 'author', 'eccompTransactionDetail'])
            ->orderBy('id', 'desc')
            ->get();
        
        $totalTransaction = 0;
        $transactions->map(function ($item) use(&$totalTransaction){
            $totalTransaction += $item->eccompTransactionDetail->sum('total_price');
        });

        return view('sales.index', compact('transactions', 'totalTransaction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = EccompCustomer::get();
        $products = EccompProduct::where('stock', '>', 0)->get();
        return view('sales.create', compact('customers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSalesOrderRequest $request)
    {
        try {
            DB::beginTransaction();
            $transaction = EccompTransaction::create([
                'date' => Carbon::parse($request->tanggal)->format('Y-m-d'),
                'eccomp_customer_id' => $request->customer,
                'eccomp_user_id' => auth()->user()->id
            ]);
            
            foreach ($request->produk as $key => $value) {
                $product = EccompProduct::where('code', $request->produk[$key])->firstOrFail();
                $transaction->eccompTransactionDetail()->create([
                    'eccomp_product_code' => $request->produk[$key], 
                    'qty' => $request->qty[$key], 
                    'unit_price' => $product->sell_price, 
                    'total_price' => $request->qty[$key] * $product->sell_price
                ]);
            }
            DB::commit();
            return redirect()->route('sales.index')->with('success', 'Transaksi berhasil dibuat');
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage());
            return redirect()->back()->with('error', 'Transaksi gagal dibuat');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = EccompTransaction::with(['customer', 'author', 'eccompTransactionDetail.product'])
            ->where('id', $id)
            ->firstOrFail();
        return view('sales.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = EccompTransaction::with(['customer', 'author', 'eccompTransactionDetail.product'])
            ->where('id', $id)
            ->firstOrFail();
        $customers = EccompCustomer::get();
        $products = EccompProduct::where('stock', '>', 0)->get();

        return view('sales.edit', compact('transaction', 'customers', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSalesOrderRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $transaction = EccompTransaction::with(['eccompTransactionDetail'])->where('id', $id)->firstOrFail();
            $transactionDetails = $transaction->eccompTransactionDetail->pluck('id');
            EccompTransactionDetail::whereIn('id', $transactionDetails)->delete();

            $transaction->date = Carbon::parse($request->tanggal)->format('Y-m-d');
            $transaction->eccomp_customer_id = $request->customer;
            $transaction->save();

            foreach ($request->produk as $key => $value) {
                $product = EccompProduct::where('code', $request->produk[$key])->firstOrFail();
                $transaction->eccompTransactionDetail()->create([
                    'eccomp_product_code' => $request->produk[$key],
                    'qty' => $request->qty[$key],
                    'unit_price' => $product->sell_price,
                    'total_price' => $request->qty[$key] * $product->sell_price
                ]);
            }
            DB::commit();
            return redirect()->route('sales.index')->with('success', 'Transaksi berhasil dibuat');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Transaksi gagal dibuat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteSalesOrderRequest $request)
    {
        EccompTransaction::find($request->id)->delete();
        return redirect()->back()->with('success', 'Hapus transaksi penjualan berhasil');
    }

    /**
     * Upload file import
     *
     * @param UploadImportRequest $request
     * @return void
     */
    public function uploadImport(UploadImportRequest $request)
    {
        $upload = $this->importSalesOrder->uploadImport($request);

        return response()->json($upload);
    }

    /**
     * Execute import
     *
     * @param ExecuteImportRequest $request
     * @return void
     */
    public function executeImport(ExecuteImportRequest $request)
    {
        $execute = $this->importSalesOrder->executeImport($request);

        return response()->json($execute);
    }
}
