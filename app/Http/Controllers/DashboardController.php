<?php

namespace App\Http\Controllers;

use App\Models\EccompPurchaseOrderDetail;
use App\Models\EccompTransaction;
use App\Models\EccompTransactionDetail;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $salesOrderToday = EccompTransactionDetail::whereHas('transaction', function ($q) {
            $q->where('date', Carbon::now()->format('Y-m-d'));
        })->sum('total_price');

        $purchaseOrderToday = EccompPurchaseOrderDetail::whereHas('purchaseOrder', function ($q) {
            $q->where('date', Carbon::now()->format('Y-m-d'));
        })->sum('total_price');
        
        $latestTransaction = EccompTransaction::orderBy('date', 'desc')
            ->with(['eccompTransactionDetail', 'customer'])
            ->limit(10)
            ->get();
        $latestTransaction->map(function ($item) {
            $item->total = $item->eccompTransactionDetail->sum('total_price');
        });
        
        return view('dashboard.index', compact('latestTransaction', 'salesOrderToday', 'purchaseOrderToday'));
    }


    public function dailyChart()
    {
        $chartByMonth = DB::table('eccomp_sales_daily_report')
            ->whereRaw("DATE_FORMAT(date,'%Y-%m') = '". Carbon::now()->format('Y-m')."'")
            ->get();

        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');

        $periodMonth = CarbonPeriod::create($start, $end);

        $result = [];
        foreach ($periodMonth as $period) {
            $chart = $chartByMonth->where('date', $period->format('Y-m-d'))->first();
            if (empty($chart)) {
                $result[] = [
                    'date' => $period->format('Y-m-d'),
                    'total' => 0
                ];
            }else {
                $result[] = [
                    'date' => $period->format('Y-m-d'),
                    'total' => $chart->total
                ];

            }
        }

        return response()->json($result);
    }

    public function prosentaseChart()
    {
        $transactionByCategory = DB::table('eccomp_sales_monthly_report')
            ->where('month_trans', Carbon::now()->format('m'))
            ->get();

        $allProduct =  $transactionByCategory->sum('total');
        $transactionByCategory->map(function ($item) use($allProduct) {
            $item->prosentase = round($item->total / $allProduct * 100);
        });

        return response()->json($transactionByCategory);
    }

    public function sixMonthChartTransaction()
    {
        $start = Carbon::now()->subMonth(5)->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');

        $transactionLastSixMonth = DB::table('eccomp_detail_sales_order')
            ->selectRaw("SUM(eccomp_detail_sales_order.total_price) AS total, DATE_FORMAT(eccomp_detail_sales_order.`date`, '%m') as month")
            ->whereBetween('date', [$start, $end])
            ->groupBy('month')
            ->get();

        $periodMonth = CarbonPeriod::create($start, '1 month', $end);

        $result = [];
        foreach ($periodMonth as $key => $month) {
            $transaction = $transactionLastSixMonth->where('month', $month->format('m'))->first();
            if (empty($transaction)) {
                $result[] = [
                    'date' => $month->format('m-Y'),
                    'total' => 0
                ];
            } else {
                $result[] = [
                    'date' => $month->format('m-Y'),
                    'total' => $transaction->total
                ];
            }
        }
        
        return response()->json($result);
    }
}
