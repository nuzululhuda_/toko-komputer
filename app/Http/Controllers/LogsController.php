<?php

namespace App\Http\Controllers;

use App\Models\EccompPurchaseOrder;
use App\Models\EccompTransaction;

class LogsController extends Controller
{
    public function index()
    {
        $purchaseOrders = EccompPurchaseOrder::with(['author'])->get();
        $salesOrders = EccompTransaction::with(['author'])->get();
        
        return view('logs.index', compact('purchaseOrders', 'salesOrders'));
    }
}
