<?php

namespace App\Events;

use App\Models\EccompImportLog;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ImportProgressEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $importId;
    /**
     * The name of the queue connection to use when broadcasting the event.
     *
     * @var string
     */
    public $connection = 'redis';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($importId)
    {
        $this->importId = $importId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ImportProgress');
    }

    /*
     * The Event's broadcast name.
     * 
     * @return string
     */
    public function broadcastAs()
    {
        return 'ImportProgressMessage';
    }

    /*
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $importLog = EccompImportLog::find($this->importId);
        $percentage = round(($importLog->success_import + $importLog->error_import) / $importLog->total_data * 100);
        $importLog->percentage = $percentage;
        return [
            'data' => $importLog
        ];
    }
}
