<?php

namespace App\Rules\Purchase;

use App\Models\EccompPurchaseOrder;
use Illuminate\Contracts\Validation\Rule;

class ValidToDeletePurchaseOrder implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $purchaseOrder = EccompPurchaseOrder::whereId($value)
            ->with('eccompPurchaseOrderDetail.product')
            ->first();
        
        foreach ($purchaseOrder->eccompPurchaseOrderDetail as $item) {
            $purchase = $item->qty;
            $stock = $item->product->stock;
            $qtyAfterDelete = $stock - $purchase;
            if ($qtyAfterDelete < 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tidak dapat menghapus pembelian, stok produk kurang.';
    }
}
