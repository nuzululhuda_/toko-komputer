<?php
namespace App\Import;

use Carbon\Carbon;
use App\Models\EccompProduct;
use App\Models\EccompCustomer;
use App\Models\EccompTransaction;

class ImportSalesOrder extends ImportSalesOrderBase
{
    public $dateIndex = [0];
    public $uploadFolder = 'sales-order';

    /**
     * covert row to associate array
     *
     * @param [type] $row
     * @return void
     */
    public function rowToAssociateArray($row)
    {
        $data['date'] = $row[0];
        $data['customer_name'] = $row[1];
        $data['customer_address'] = $row[2];
        $data['product_code'] = $row[3];
        $data['qty'] = $row[4];

        return $data;
    }

    /**
     * Validate every row
     *
     * @param $row
     * @return void
     */
    public function validateRow($row)
    {
        $valid = $this->validateTransactionDate($row['date']);
        if (!$valid) {
            return false;
        }

        $valid = $this->validateCustomer($row['customer_name'], $row['customer_address']);
        if (!$valid) {
            return false;
        }

        $valid = $this->validateProductCode($row['product_code']);
        if (!$valid) {
            return false;
        }

        $valid = $this->validateQty($row['product_code'], $row['qty']);
        if (!$valid) {
            return false;
        }

        return true;
    }
    

    /**
     * Validasi tanggal
     *
     * @param [type] $date
     * @return void
     */
    private function validateTransactionDate($date)
    {
        try {
            Carbon::createFromFormat('d/m/Y', $date)->format('d-m-Y');
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Validasi data customer
     * @param $name
     * @param $address
     * @return void
     */
    private function validateCustomer($name, $address)
    {
        if (strlen($name) < 1 || strlen($address) < 1) {
            return false;
        }
        return true;
    }

    /**
     * Validasi kode produk
     *
     * @param $productCode
     * @return void
     */
    private function validateProductCode($productCode)
    {
        if (strlen($productCode) < 2) {
            return false;
        }

        $exists = EccompProduct::where('code', $productCode)->exists();
        if (!$exists) {
            return false;
        }

        return true;
    }

    /**
     * Validate qty of product 
     *
     * @param $productCode
     * @param $qty
     * @return void
     */
    private function validateQty($productCode, $qty)
    {
        if (! is_int($qty)) {
            return false;
        }

        $product = EccompProduct::where('code', $productCode)->first();
        if ($product->stock < $qty) {
           return false;
        }

        return true;
    }

    /**
     * Check transaction exists or not
     *
     * @param $customerId
     * @param $date
     * @return void
     */
    public function getTransaction($customerId, $date)
    {
        $date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        $transaction = EccompTransaction::where('date', $date)
            ->where('eccomp_customer_id', $customerId)
            ->with(['eccompTransactionDetail'])
            ->first();
        return $transaction;
    }

    public function getOrCreateCustomer($customerName, $customerAddress)
    {
        $customer = EccompCustomer::firstOrCreate([
            'name' => $customerName,
            'address' => $customerAddress
        ]);
        return $customer;
    }
}
