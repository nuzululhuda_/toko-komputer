<?php
namespace App\Import;

use Carbon\Carbon;
use App\Models\EccompProduct;
use App\Models\EccompPurchaseOrder;
use App\Import\ImportPurchaseOrderBase;

class ImportPurchaseOrder extends ImportPurchaseOrderBase
{
    public $uploadFolder = 'purchase-order';
    public $dateIndex = [0];

    /**
     * covert row to associate array
     *
     * @param [type] $row
     * @return void
     */
    public function rowToAssociateArray($row)
    {
        $data['date'] = $row[0];
        $data['product_code'] = $row[1];
        $data['qty'] = $row[2];
        return $data;
    }

    /**
     * validate every row
     *
     * @param [type] $row
     * @return void
     */
    public function validateRow($row)
    {
        $valid = $this->validatePurchaseDate($row['date']);
        if (!$valid) {
            return false;
        }

        $valid = $this->validateProductCode($row['product_code']);
        if (!$valid) {
            return false;
        }

        $valid = $this->validateQty($row['qty']);
        if (!$valid) {
            return false;
        }

        return true;
    }

    /**
     * validate purchase date
     *
     * @param $date
     * @return void
     */
    private function validatePurchaseDate($date)
    {
        try {
            Carbon::createFromFormat('d/m/Y', $date)->format('d-m-Y');
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * validate product code
     *
     * @param $productCode
     * @return void
     */
    private function validateProductCode($productCode)
    {
        if (strlen($productCode) < 2) {
            return false;
        }

        $exists = EccompProduct::where('code', $productCode)->exists();
        if (!$exists) {
            return false;
        }

        return true;
    }

    /**
     * validate qty
     *
     * @param $qty
     * @return void
     */
    private function validateQty($qty)
    {
        if (!is_int($qty)) {
            return false;
        }

        return true;
    }

    /**
     * get purchase transaction
     *
     * @param $date
     * @return void
     */
    public function getPurchaseTransaction($date)
    {
        $date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        $purchaseTransaction = EccompPurchaseOrder::where('date', $date)->first();
        if (!$purchaseTransaction) {
            return false;
        }
        
        return $purchaseTransaction;
    }
}
