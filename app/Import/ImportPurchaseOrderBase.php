<?php
namespace App\Import;

use Carbon\Carbon;
use App\Import\ImportBase;
use App\Models\EccompProduct;
use App\Models\EccompPurchaseOrder;

abstract class ImportPurchaseOrderBase extends ImportBase
{
    /**
     * Execute import purchase transaction
     *
     * @param $row
     * @return void
     */
    public function executeImportRow($row)
    {
        $purchaseTransaction = $this->getPurchaseTransaction($row['date']);

        if ($purchaseTransaction) {
            $this->addDetailPurchaseTransaction($purchaseTransaction, $row);
        }else {
            $this->createPurchaseTransaction($row);
        }
    }

    /**
     * Add detail purchase order
     *
     * @param $purchaseTransaction
     * @param $row
     * @return void
     */
    private function addDetailPurchaseTransaction($purchaseTransaction, $row)
    {
        $produk = EccompProduct::whereCode($row['product_code'])->first();
        $purchaseTransaction->eccompPurchaseOrderDetail()->create([
            'eccomp_product_code' => $produk->code,
            'qty' => $row['qty'],
            'total_price' => $row['qty'] * $produk->purchase_price
        ]);
        return true;
    }

    /**
     * Create new purchase transaction
     *
     * @param [type] $row
     * @return void
     */
    private function createPurchaseTransaction($row)
    {
        $purchaseTransaction = EccompPurchaseOrder::create([
            'date' => Carbon::createFromFormat('d/m/Y', $row['date'])->format('Y-m-d'), 
            'eccomp_user_id' => auth()->user()->id
        ]);

        $produk = EccompProduct::whereCode($row['product_code'])->first();
        $purchaseTransaction->eccompPurchaseOrderDetail()->create([
            'eccomp_product_code' => $produk->code, 
            'qty' => $row['qty'],
            'total_price' => $row['qty'] * $produk->purchase_price
        ]);
        return true;
    }
}
