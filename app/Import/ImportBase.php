<?php 
namespace App\Import;

use App\Events\ImportProgressEvent;
use App\Models\EccompImportLog;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

abstract class ImportBase{
    protected $uploadFolder; // diupload di folder mana 

    abstract public function validateRow($row); // validasi satu row
    abstract public function executeImportRow($row); // import 1 row
    abstract public function rowToAssociateArray($row); // ubah row[1] ke row['name]

    /**
     * Upload import
     *
     * @param $request
     * @return void
     */
    public function uploadImport($request)
    {
        $fileUploaded = $this->uploadFile($request);
        if (!$fileUploaded) {
            return [
                'error' => true,
                'message' => 'Gagal upload file'
            ];
        }

        $rows = $this->importToArray($fileUploaded);

        $result = $this->validateRows($rows);
        $filenameArray = explode("/", $fileUploaded);
        $result['filename'] = end($filenameArray);
        return $result;
    }

    /**
     * Upload file to spesific folder
     *
     * @param $request
     * @return void
     */
    private function uploadFile($request)
    {
        $filename = date("ymd") . '-' . date("his") . "." . $request->file->extension();
        $folder =  'public/' . $this->uploadFolder;
        $uploaded = $request->file->storeAs($folder, $filename);

        if ($uploaded) {
            return Storage::path($uploaded);
        }

        return false;
    }

    /**
     * Convert upload import
     *
     * @param $path
     * @return void
     */
    private function importToArray($path)
    {
        $data = Excel::toArray(new ImportToArray(), $path)[0];
        unset($data[0]);

        if (isset($this->dateIndex)) {
            $data = $this->parseDate($data);
        }

        return $data;
    }

    /**
     * Parse input date format
     *
     * @param $data
     * @return void
     */
    private function parseDate($data)
    {
        foreach ($data as &$allData) {
            foreach ($allData as $key => $item) {
                if (in_array($key, $this->dateIndex)) {
                    if (is_float($item) || is_int($item)) {
                        $allData[$key] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item)->format('d/m/Y');
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Validate all rows
     *
     * @param $rows
     * @return void
     */
    private function validateRows($rows)
    {
        $totalValid = 0;
        $totalInvalid = 0;
        $validData = [];
        $invalidData = [];

        foreach ($rows as $row) {
            $formattedRow = $this->rowToAssociateArray($row);
            $valid = $this->validateRow($formattedRow);

            if ($valid) {
                $totalValid++;
                $validData[] = $formattedRow;
            }else {
                $totalInvalid++;
                $invalidData[] = $formattedRow;
            }
        }

        return [
            'valid' => $totalValid,
            'invalid' => $totalInvalid,
            'valid_data' => $validData,
            'invalid_data' => $invalidData,
            'total' => $totalValid + $totalInvalid
        ];
    }

    /**
     * Execute import file
     *
     * @param $request
     * @return void
     */
    public function executeImport($request)
    {
        $folder =  'public/' . $this->uploadFolder;
        $fileUploaded = Storage::path($folder . '/' . $request->filename);
        $rows = $this->importToArray($fileUploaded);

        $result = $this->validateRows($rows);
        $importLog = $this->createStatsImport($result);
        foreach ($result['valid_data'] as  $row) {
            $importResult = $this->executeImportRow($row);
            if (!$importResult) {
                $this->addErrorImportStats($importLog->id);
            }else {
                $this->addSuccessImportStats($importLog->id);
            }
            broadcast(new ImportProgressEvent($importLog->id));
        }

        return true;
    }

    /**
     * initialize import stats
     *
     * @param [type] $data
     * @return void
     */
    private function createStatsImport($data)
    {
        $importLog = EccompImportLog::create([
            'import_name' => 'import-' . $this->uploadFolder,
            'total_data' => $data['valid'],
        ]);
        return $importLog;
    }

    /**
     * Add error import stats
     *
     * @param  $importLogId
     * @return void
     */
    private function addErrorImportStats($importLogId)
    {
        $importLog = EccompImportLog::find($importLogId);
        $totalImported = ($importLog->success_import+1) + $importLog->error_import;
        if ($totalImported ==  $importLog->total_data) {
            $importLog->done_at = Carbon::now();
        }
        $importLog->success_import = $importLog->success_import+1;
        $importLog->save();
    }

    /**
     * Add error import stats
     *
     * @param  $importLogId
     * @return void
     */
    private function addSuccessImportStats($importLogId)
    {
        $importLog = EccompImportLog::find($importLogId);
        $totalImported = ($importLog->error_import + 1) + $importLog->success_import;
        if ($totalImported ==  $importLog->total_data) {
            $importLog->done_at = Carbon::now();
        }
        $importLog->error_import = $importLog->error_import + 1;
        $importLog->save();
    }

    
}