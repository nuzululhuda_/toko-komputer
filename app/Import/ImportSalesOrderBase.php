<?php

namespace App\Import;

use App\Models\EccompProduct;
use App\Models\EccompTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

abstract class ImportSalesOrderBase extends ImportBase
{
    /**
     * Execute import every row
     *
     * @param $row
     * @return void
     */
    public function executeImportRow($row)
    { 
        $customer = $this->getOrCreateCustomer($row['customer_name'], $row['customer_address']);
        $transaction = $this->getTransaction($customer->id, $row['date']);

        if ($transaction) {
            $this->addDetailTransaction($transaction, $row);
        }else {
            $this->createTransaction($row, $customer);
        }
    }

    public function createTransaction($row, $customer)
    {
        try {
            $transaction = EccompTransaction::create([
                'date' => Carbon::createFromFormat('d/m/Y', $row['date'])->format('Y-m-d'), 
                'eccomp_customer_id' => $customer->id, 
                'eccomp_user_id' => auth()->user()->id
            ]);

            $product = EccompProduct::whereCode($row['product_code'])->first();
            $transaction->eccompTransactionDetail()->create([
                'eccomp_product_code' => $product->code, 
                'qty' => $row['qty'], 
                'unit_price' => $product->sell_price, 
                'total_price' => $row['qty'] * $product->sell_price
            ]);
            return $transaction;
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function addDetailTransaction($transaction, $row)
    {
       try {
            $product = EccompProduct::whereCode($row['product_code'])->first();
            $transaction->eccompTransactionDetail()->create([
                'eccomp_product_code' => $product->code,
                'qty' => $row['qty'],
                'unit_price' => $product->sell_price,
                'total_price' => $row['qty'] * $product->sell_price
            ]);
            return $transaction;
       } catch (\Throwable $th) {
           return false;
       }
        
    }
}
