<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EccompTransaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'eccomp_customer_id', 'eccomp_user_id'];

    /**
     * Relation to transaction detail
     *
     * @return void
     */
    public function eccompTransactionDetail()
    {
        return $this->hasMany(EccompTransactionDetail::class, 'eccomp_transaction_id');
    }

    public function customer()
    {
        return $this->belongsTo(EccompCustomer::class, 'eccomp_customer_id');
    }

    public function author()
    {
        return $this->belongsTo(EccompUser::class, 'eccomp_user_id');
    }
}
