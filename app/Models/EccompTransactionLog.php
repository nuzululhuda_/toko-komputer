<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EccompTransactionLog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'stock',
        'total_sales',
        'total_purchase',
        'eccomp_product_code',
    ];

    /**
     * Relation to product
     *
     * @return void
     */
    public function product()
    {
        return $this->belongsTo(EccompProduct::class, 'eccomp_product_code', 'code');
    }
}
