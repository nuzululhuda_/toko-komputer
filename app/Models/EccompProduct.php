<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EccompProduct extends Model
{
    use HasFactory;
    const CATEGORIES = ['Apple', 'Lenovo', 'Asus', 'HP', 'Thosiba'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name', 'sell_price', 'purchase_price', 'stock', 'category'];
}
