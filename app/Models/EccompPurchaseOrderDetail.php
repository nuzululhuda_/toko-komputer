<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EccompPurchaseOrderDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['eccomp_purchase_order_id', 'eccomp_product_code', 'qty', 'total_price'];

    public function product()
    {
        return $this->belongsTo(EccompProduct::class, 'eccomp_product_code', 'code');
    }

    public function purchaseOrder()
    {
        return $this->belongsTo(EccompPurchaseOrder::class, 'eccomp_purchase_order_id');
    }
}
