<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EccompSalesMonthlyReport extends Model
{
    use HasFactory;
    protected $table = 'eccomp_sales_monthly_report';
}
