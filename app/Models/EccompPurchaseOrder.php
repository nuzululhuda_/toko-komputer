<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EccompPurchaseOrder extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'eccomp_user_id'];

    public function eccompPurchaseOrderDetail()
    {
        return $this->hasMany(EccompPurchaseOrderDetail::class);
    }

    public function author()
    {
        return $this->belongsTo(EccompUser::class, 'eccomp_user_id');
    }
}
