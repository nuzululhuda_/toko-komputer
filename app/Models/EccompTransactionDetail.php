<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EccompTransactionDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['transaction_id', 'eccomp_product_code', 'qty', 'unit_price', 'total_price'];

    /**
     * Relation to transaction
     *
     * @return void
     */
    public function transaction()
    {
        return $this->belongsTo(EccompTransaction::class, 'eccomp_transaction_id');
    }

    /**
     * Relation to product
     *
     * @return void
     */
    public function product()
    {
        return $this->belongsTo(EccompProduct::class, 'eccomp_product_code', 'code');
    }
}
