<?php

use App\Models\EccompTransactionLog;

if (!function_exists('log_transaction')) {
    function log_transaction($productCode, $date, $totalPurchase, $totalSales) 
    {
        $log = EccompTransactionLog::where('eccomp_product_code', $productCode)
            ->where('date', $date)
            ->first();
        
        if ($log) {
            $log->total_purchase = $log->total_purchase + $totalPurchase;
            $log->total_sales = $log->total_sales + $totalSales;
            $log->save();
        }else {
            EccompTransactionLog::create([
                'date' => $date,
                'eccomp_product_code' => $productCode,
                'total_purchase' => $totalPurchase,
                'total_sales' => $totalSales
            ]);
        }
        return true;
    }
}