<?php

use App\Http\Controllers\LogsController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SalesOrderController;
use App\Http\Controllers\PurchaseOrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('dashboard')->middleware('auth:web')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('dailychart', [DashboardController::class, 'dailyChart'])->name('dashboard.chart.daily');
    Route::get('prosentasechart', [DashboardController::class, 'prosentaseChart'])->name('dashboard.chart.prosentase');
    Route::get('lastsixmonth', [DashboardController::class, 'sixMonthChartTransaction'])->name('dashboard.chart.sixmonth');
    Route::get('logs', [LogsController::class, 'index'])->name('logs.index');
    Route::get('/report/profit', [ReportController::class, 'profit'])->name('report.profit');
    Route::get('/report/stockcard', [ReportController::class, 'stockCard'])->name('report.stock-card');
    Route::get('/report/transaction', [ReportController::class, 'transaction'])->name('report.transaction');

    Route::resource('purchases', PurchaseOrderController::class)->except(['destroy']);
    Route::delete('purchases/destroy', [PurchaseOrderController::class, 'destroy'])->name('purchases.destroy');
    Route::post('purchase/upload-import', [PurchaseOrderController::class, 'uploadImport'])->name('purchases.upload.import');
    Route::post('purchase/execute-import', [PurchaseOrderController::class, 'executeImport'])->name('purchases.execute.import');
    
    Route::resource('sales', SalesOrderController::class)->except(['destroy']);
    Route::delete('sales/destroy', [SalesOrderController::class, 'destroy'])->name('sales.destroy');
    Route::post('sales/upload-import', [SalesOrderController::class, 'uploadImport'])->name('sales.upload.import');
    Route::post('sales/execute-import', [SalesOrderController::class, 'executeImport'])->name('sales.execute.import');
    
    Route::resource('products', ProductController::class);
    Route::post('producsts/stock-card', [ProductController::class, 'stockCard'])->name('products.stock-card');
    
});

Route::get('/send', function () {
    broadcast(new App\Events\EveryoneEvent());
    return response('Sent');
});

Route::get('/receiver', function () {
    return view('receiver');
});

require __DIR__.'/auth.php';
